Contributions to the `irc3` crate must be licensed under [The Unlicense](https://unlicense.org). This may change in the future.

Be courteous, constructive and respectful when talking over pull requests and issues.
