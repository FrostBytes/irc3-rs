# IRC3 for Rust
Good, Rusty IRC for the humans.

## DISCLAIMER
If you're looking for a time-tested library, `irc3` is not for you. Releases are coming out every day with new bugs
and other scary issues. Please go check out the [irc](https://crates.io/crates/irc) crate instead. Otherwise, happy
debugging!

# Writing a simple pinger
Get started by installing [irc3](https://crates.io/crates/irc3) and an async runtime
([tokio](https:://crates.io/crates/tokio) in this example). You will also need the 
[futures](https:://crates.io/crates/futures) abstractions to directly interact with the `Stream`/`Sink` API.
```toml
irc3 = "0.2"
tokio = "0.2"
futures = "0.3"
```

You first begin by creating an **unsecure** client with a `Client::new(...)`, and then sending any necessary
authentication messages (such as `PASS`, `NICK` and `USER`). Then, just listen for any incoming messages that
have a `PING` command, and reply with a `PONG` command. The commands will look similar to this:
```
> PING :tmi.twitch.tv
< PONG :tmi.twitch.tv
```

You can implement this in Rust:
```rust
extern crate tokio;
extern crate irc3;
extern crate futures;

use std::env::var;

use irc3::{Client, Message};

use futures::stream::StreamExt as _;
use futures::sink::SinkExt as _;

#[tokio::main]
async fn main() {
	// build a client
	let mut client = Client::new("irc.chat.twitch.tv").await.unwrap();

	client.send(Message::new("PASS").with_param(&var("IRC3_RS_PASSWORD").unwrap())).await.unwrap();
	client.send(Message::new("NICK").with_param(&var("IRC3_RS_NICKNAME").unwrap())).await.unwrap();

	loop {
		let message = match client.next().await {
			Some(res) => res,
			None => break,
		}.unwrap();

		if message.command() == "PING" {
			client.send(Message::new("PONG").with_param(message.params().last().unwrap())).await.unwrap();
		}
	}
}
```

## SSL connections
To use SSL, you must enable the `tls` feature of this crate. You may use the `Client::new_secure(...)`
function to create TLS encrypted connections. All the TLS is handled at the time of connection, so you 
don't have to type anything extra.

# Writing Servers
The `irc3` crate wants to support servers, but the code isn't there yet. However, the technology is there.
Using the `MessageTransport` struct, you can wait and then accept connections from clients.

If you want the `Server` abstractions, come back later! Or, you can [come and help](CONTRIBUTING.md)!
