use std::io::Error as IoError;
use std::pin::Pin;
use std::borrow::Cow;

use futures::io::{AsyncRead, AsyncWrite};
use futures::task::{Context, Poll};

use async_std::net::{TcpStream};

#[cfg(feature = "tls")]
use async_tls::TlsConnector;
#[cfg(feature = "tls")]
use async_tls::client::TlsStream;

fn normalize<'a, S: AsRef<str> + ?Sized>(dest: &'a S, port: &str) -> Cow<'a, str> {
	let mut dest = Cow::from(dest.as_ref());

	if dest.find(':').is_none() {
		let dest = dest.to_mut();
		dest.push(':');
		dest.push_str(port);
	}

	dest
}

/// The underlying Connection of a client.
pub enum Connection {
	Unsecure(TcpStream),
	#[cfg(feature = "tls")]
	Secure(TlsStream<TcpStream>),
}

impl Connection {
	/// Creates an unsecured (plaintext) connection to an IRC server.
	///
	/// The function expects a resolvable IP / DOMAIN in the form `<ip>[:<port>]` where `<port>` is
	/// optional.
	pub async fn unsecure<S: AsRef<str> + ?Sized>(dest: &S) -> Result<Connection, IoError> {
		Ok(Connection::Unsecure(TcpStream::connect(normalize(dest, "6667").as_ref()).await?))
	}

	/// Creates a secured (TLS) connection to an IRC server.
	///
	/// The function expects a resolvable DOMAIN in the form `<domain>[:<port>]` where `<port>` is
	/// optional.
	#[cfg(feature = "tls")]
	pub async fn secure<S: AsRef<str> + ?Sized>(dest: &S) -> Result<Connection, IoError> {
		// instantiate a tlsconnector and dest
		let dest = normalize(dest, "6697");
		let connector = TlsConnector::default();
		
		// build tcp stream
		let tcp = TcpStream::connect(dest.as_ref()).await?;

		// attempt a TLS handshake
		Ok(Connection::Secure(connector.connect(dest.as_ref().split(':').next().unwrap(), tcp)?.await?))
	}
}

impl AsyncRead for Connection {
	fn poll_read(mut self: Pin<&mut Self>, cx: &mut Context, buf: &mut [u8]) -> Poll<Result<usize, IoError>> {
		match *self {
			Connection::Unsecure(ref mut st) => Pin::new(st).poll_read(cx, buf),
			#[cfg(feature = "tls")]
			Connection::Secure(ref mut sts) => Pin::new(sts).poll_read(cx, buf),
		}
	}
}

impl AsyncWrite for Connection {
	fn poll_write(mut self: Pin<&mut Self>, cx: &mut Context, buf: &[u8]) -> Poll<Result<usize, IoError>> {
		match *self {
			Connection::Unsecure(ref mut st) => Pin::new(st).poll_write(cx, buf),
			#[cfg(feature = "tls")]
			Connection::Secure(ref mut sts) => Pin::new(sts).poll_write(cx, buf),
		}

	}

	fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Result<(), IoError>> {
		match *self {
			Connection::Unsecure(ref mut st) => Pin::new(st).poll_flush(cx),
			#[cfg(feature = "tls")]
			Connection::Secure(ref mut sts) => Pin::new(sts).poll_flush(cx),
		}
	}

	fn poll_close(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Result<(), IoError>> {
		match *self {
			Connection::Unsecure(ref mut st) => Pin::new(st).poll_close(cx),
			#[cfg(feature = "tls")]
			Connection::Secure(ref mut sts) => Pin::new(sts).poll_close(cx),
		}
	}
}
