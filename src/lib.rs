//! # IRC3 for Rust
//! The Rust IRC3 crate. Fast, async IRC for the humans.
//! The IRC3 is designed to be an alternative to the [IRC
//! crate](https://crates.io/crates/irc) that's low level
//! and optimized.
//!
//! # DISCLAIMER
//! As of `0.2.0` of the `irc3` crate, features documented
//! may not be available.
//! 
//! # Examples
//! ```ignore
//! extern crate irc3;
//! extern crate tokio;
//!
//! use irc3::{Client, Message};
//! 
//! #[tokio::main]
//! async fn main() {
//! 	let mut client = Client::new("localhost").await.unwrap();
//!
//! 	client.send(Message::new("NICK").with_param("frostu8")).await.unwrap();
//! 	client.flush();
//!		
//! 	// prints the server's welcome message
//! 	client.next().await.unwrap().params().last();
//! }
//! ```

extern crate futures;
extern crate async_std;
#[cfg(feature = "tls")]
extern crate async_tls;

pub mod client;
pub mod proto;

pub use proto::message::Message;
pub use client::Client;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
