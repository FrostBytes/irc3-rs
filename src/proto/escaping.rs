pub fn unescape<S: AsRef<str> + ?Sized>(s: &S) -> String {
	// get string ref
	let string = s.as_ref();

	let mut buf = String::with_capacity(string.len());

	// iterate through the string
	let mut cursor: usize = 0;

	while cursor < string.len() {
		let ch = string[cursor..].chars().next().unwrap();

		if ch == '\\' {
			// escape found!!
			cursor += ch.len_utf8();

			if cursor < string.len() {
				let ch = string[cursor..].chars().next().unwrap();

				match ch {
					':' => buf.push(';'),
					's' => buf.push(' '),
					'r' => buf.push('\r'),
					'n' => buf.push('\n'),
					_ => buf.push(ch),
				}
			}
		} else {
			buf.push(ch);
		}

		cursor += ch.len_utf8();
	}

	buf
}

pub fn escape<S: AsRef<str> + ?Sized>(s: &S) -> String {
	// get string ref
	let string = s.as_ref();

	let mut buf = String::with_capacity(string.len());

	for c in string.chars() {
		match c {
			';' => buf.push_str("\\:"),
			' ' => buf.push_str("\\s"),
			'\\' => buf.push_str("\\\\"),
			'\r' => buf.push_str("\\r"),
			'\n' => buf.push_str("\\n"),
			c => buf.push(c),
		}
	}

	buf
}
